package main

import (
    "fmt"
    "log"
    "math"
    "math/rand"
    "os"
    "sort"
    "time"
)

type (
    node struct {
        row int
        column int
        obstacle bool
        G, H, F int
        parent *node
    }

    // error type
    err_meet_end struct {
    }

    nodelist []*node
)

/*
 * G 是节点到起点的距离
 * H 是节点到终点的估算距离
 * F = G + H
 * openlist 中保存未探测的节点
 * closelist 中保存已探测的节点
 */
var (
    row, column = 15, 15
    start_node, end_node *node = nil, nil
    node_map = make([][]node, row)
    openlist = nodelist{}
    closelist = nodelist{}
    G = 1
)

// 打印格子, 障碍, 起点终点, 路径
func print() {
    fmt.Printf("   ")
    for m := 0; m < column; m++ {
        fmt.Printf("  %02d", m)
    }
    fmt.Printf("\n")

    for n := 0; n < row; n++ {
        fmt.Printf("   ")
        for m := 0; m < column; m++ {
            fmt.Printf(" ---")
        }
        fmt.Printf("\n")
        fmt.Printf("%02d ", n)

        for m := 0; m < column; m++ {
            if &node_map[n][m] == start_node {
                fmt.Printf("| S ")
            } else if &node_map[n][m] == end_node {
                fmt.Printf("| E ")
            } else if node_map[n][m].obstacle {
                fmt.Printf("| + ")
            } else if in(&node_map[n][m], closelist) {
                index := getindex(&node_map[n][m], closelist)
                fmt.Printf("|%2d ", len(closelist) - index - 1)
            } else {
                fmt.Printf("|   ")
            }
        }
        fmt.Printf("|\n")

        if n == row - 1 {
            fmt.Printf("   ")
            for m := 0; m < column; m++ {
                fmt.Printf(" ---")
            }
            fmt.Printf("\n")
        }
    }
}

func calculateGHF(n1, n2 *node, G int) (int, int){
    h := int(math.Abs(float64(n2.column - n1.column)) + math.Abs(float64(n2.row - n1.row)))
    f := h + G
    return h, f
}

func (e err_meet_end) Error() string {
    return ""
}

func in(no* node, l nodelist) bool {
    for _, v := range l {
        if no.column == v.column && no.row == v.row {
            return true
        }
    }
    return false
}

func (a nodelist) Len() int {
    return len(a)
}

func (a nodelist) Swap(i, j int) {
    a[i], a[j] = a[j], a[i]
}

func (a nodelist) Less(i, j int) bool {
    return a[i].F < a[j].F
}

func removelist(no *node, l *nodelist) {
    index := getindex(no, *l)
    tmp := (*l)[ :index]
    tmp = append(tmp, (*l)[index + 1: ]...)
    *l = tmp
}

// func sortlist(l []*node) {
//     for n := 0; n < len(l) - 1; n++ {
//         for i := n; i < len(l) - 1; i++ {
//             if l[n].F > l[i + 1].F {
//                 l[n], l[i + 1] = l[i + 1], l[n]
//             }
//         }
//     }
// }

func getindex(no* node, l nodelist) int {
    index := -1
    for i := 0; i < len(l); i++ {
        if no == l[i] {
            index = i
            break
        }
    }

    if index == -1 {
        log.Fatal("no is not in l")
    }
    return index
}

/*
 * 参考: http://www.raywenderlich.com/zh-hans/21503/a%E6%98%9F%E5%AF%BB%E8%B7%AF%E7%AE%97%E6%B3%95%E4%BB%8B%E7%BB%8D
 */
func main() {
    rand.Seed(int64(os.Getpid()) ^ time.Now().Unix())

    // init 2D array
    for n := 0; n < len(node_map); n ++ {
        node_map[n] = make([]node, column)
        for m := 0; m < len(node_map[n]); m ++ {
            no := &node_map[n][m]
            no.row, no.column, no.obstacle = n, m, false
        }
    }

    // init start, end point
    start_node = &node_map[rand.Int() % row][rand.Int() % column]
    end_node = &node_map[rand.Int() % row][rand.Int() % column]

    // init obstacle
    for n := 0; n < len(node_map); n ++ {
        obstacle_num_per_row := (rand.Int() % (column / 3 - 1)) + 1  // 每行的障碍数 [1, column / 3]
        for {
            no := &node_map[n][rand.Int() % column]
            // start, end point 不能是 obstacle, 如果已经是 obstacle 则不会再变一次
            if no != start_node && no != end_node && no.obstacle == false {
                no.obstacle = true
                obstacle_num_per_row--
                if obstacle_num_per_row == 0 {
                    break
                }
            }
        }
    }

    // print map init stat
    print()

    filter_index := func(x, y int) bool {
        if y >= 0 && y <= row - 1 && x >= 0 && x <= column - 1 {
            return false
        }
        return true
    }

    filter_obstacle := func(no * node) bool {
        return no.obstacle
    }

    filter_in_closelist := func(no *node) bool {
        return in(no, closelist)
    }

    closelist = append(closelist, start_node)

    add_to_adjacent := func(x, y int, parent *node, adjacent *nodelist) error {
        if filter_index(x, y) {
            log.Printf("G = %d, node = (%d, %d), filter index\n", G, x, y)
            return nil
        }

        no := &node_map[y][x]
        if no == end_node {
            log.Println("meet end node")
            return err_meet_end{}
        }

        if filter_obstacle(no) {
            log.Printf("G = %d, node = (%d, %d), filter obstacle\n", G, x, y)
            return nil
        }

        if filter_in_closelist(no) {
            log.Printf("G = %d, node = (%d, %d), filter in closelist\n", G, x, y)
            return nil
        }

        // node 在 openlist 中，如果新的 F 要小于原来的 F，才改变他的 F, G, H 和 parent, 并加入到 adjacent
        if in(no, openlist) {
            h, f := calculateGHF(no, end_node, G)
            if f < no.F {
                no.H, no.G, no.F = h, G, f
                no.parent = parent
            } else {
                log.Printf("G = %d, node = (%d, %d), filter in openlist\n", G, x, y)
                return nil
            }
        }

        h, f := calculateGHF(no, end_node, G)
        no.H, no.G, no.F = h, G, f
        *adjacent = append(*adjacent, no)
        log.Printf("G = %d, node = (%d, %d)\n", G, x, y)
        return nil
    }

    for {
        /*
         * 从左上右下的方向找出四个节点, 找出其中 F 最小的节点 S，加入 closelist，其他加入到 openlist
         *     若超出地图范围，过滤
         *     若是障碍，过滤
         *     若在 closelist 中，过滤
         * 得到 S 后和 openlist 中的最小来比
         *     openlist 中最小节点的 F 比 S 的 F 小，那么要使用 openlist 中的节点为 S
         */
        no := closelist[len(closelist) - 1] // 不能用 closelist[G - 1]
        var adjacent nodelist
        if err := add_to_adjacent(no.column - 1, no.row, no, &adjacent); err != nil { // left
            if _, ok := err.(err_meet_end); ok {
                break
            } else {
                log.Fatal("unrecognized error", err)
            }
        }

        if err := add_to_adjacent(no.column, no.row - 1, no, &adjacent); err != nil { // to
            if _, ok := err.(err_meet_end); ok {
                break
            } else {
                log.Fatal("unrecognized error", err)
            }
        }

        if err := add_to_adjacent(no.column + 1, no.row, no, &adjacent); err != nil { // right
            if _, ok := err.(err_meet_end); ok {
                break
            } else {
                log.Fatal("unrecognized error", err)
            }
        }

        if err := add_to_adjacent(no.column, no.row + 1, no, &adjacent); err != nil { // bottom
            if _, ok := err.(err_meet_end); ok {
                break
            } else {
                log.Fatal("unrecognized error", err)
            }
        }

        var S *node
        // 如果周围没有节点则拿出 openlist 中最小节点为 S, 此时 S 的父亲不需要变
        // 若 openlist 为空，则结束
        if adjacent == nil {
            if len(openlist) == 0 {
                break
            } else {
                S = openlist[0]
                removelist(S, &openlist)
                G = S.G
            }
        } else {
            sort.Sort(adjacent)
            S = adjacent[0]

            if len(openlist) != 0 { // S 和 openlist 中的最小的比较
                if openlist[0].F < S.F { // 若 openlist[0] F 值更小
                    S  = openlist[0]
                    removelist(S, &openlist)
                    G = S.G
                } else { // 使 S 的父亲为 no, 如果 S 为 openlist 中成员则父亲不变
                    org_parent := S.parent
                    S.parent = no
                    log.Printf("S parent (%v) -> (%v)\n", org_parent, no)
                }
            } else {// openlist 为空则 S 的父亲变为 no
                org_parent := S.parent
                S.parent = no
                log.Printf("S parent (%v) -> (%v)\n", org_parent, no)
            }
        }

        log.Printf("S = (%d, %d)\n", S.column, S.row)

        // 临近节点加入 openlist 后改变父亲，排序
        for _, v := range adjacent {
            if S != v {
                openlist = append(openlist, v)
                org_parent := v.parent
                v.parent = no // 除 S 外的其他节点改变父亲
                log.Printf("adjacent parent (%v) -> (%v)\n", org_parent, no)
            }
        }
        sort.Sort(openlist)

        // S 加入 closelist
        closelist = append(closelist, S)
        G++
    }

    // reconstruct path
    tmp := []*node{}
    no := closelist[len(closelist) - 1]
    for {
        if no == nil {
            break
        }
        tmp = append(tmp, no)
        no = no.parent
    }
    closelist = tmp
    print()
}
